﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed = 20f;
    Rigidbody _rigidbody;
    Vector3 _velocity;
    Renderer _renderer;
    bool ballVisible = true;
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        if (ballVisible)
        {
            if (Input.GetKeyDown("space"))
            {
                Invoke("Launch", 0.5f);
                ballVisible = false;
            }
        }
    }

    void Launch()
    {
        _rigidbody.velocity = Vector3.down * speed;
    }

    void FixedUpdate()
    {
        _rigidbody.velocity = _rigidbody.velocity.normalized * speed;
        _velocity = _rigidbody.velocity;
        if (!_renderer.isVisible)
        {
            ballVisible = true;
            GameManager.Instatnce.Ball--;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        _rigidbody.velocity = Vector3.Reflect(_velocity, collision.contacts[0].normal);
    }
}
